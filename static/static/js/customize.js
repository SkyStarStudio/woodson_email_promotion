/**
 *    Customize - v0.0.1
 *    Made by Jason Zhang
 */
!(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], function($) {
            return factory(root, $);
        });
    } else if (typeof exports === 'object') {
        factory(root, require('jquery'));
    } else {
        factory(root, root.jQuery || root.Zepto);
    }
})(this, function(global, $) {

    "use strict";

    var initConsumerForm = function() {
        var idLists = {
            consumerForm: '#consumer-form'
        }

        var consumerForm = $(idLists.consumerForm);


        consumerForm
            .submit(false)
            .on('click', 'input[type=submit]', function() {

                var $others = $('.has-complementary');
                $others.each(function(index, elem) {
                    var $elem = $(elem);
                    var $complementry = $('.complementary').filter(function() {
                        return $elem.attr('complementary') === $(this).attr('complementary-id');
                    });

                    $elem.val($complementry.val());
                });


                $.ajax({
                    method: consumerForm.attr('method'),
                    url: consumerForm.attr('action'),
                    data: consumerForm.serialize(),
                    success: function(data, textStatus, jqXHR) {
                        alert(data.message);
                        location.reload();
                    },
                    error: function(jqXHR, textStatus, error) {
                        alert(error.message);
                    }
                });
            });
    };

    $(function() {

        var icons = {
            header: "glyphicon glyphicon-plus",
            activeHeader: "glyphicon glyphicon-minus"
        };

        $( "#accordion" ).accordion({
            icons: icons
        });

        initConsumerForm();

        var region_url = location.href;
        var reg = /^.*com\/\?{0,1}([A-Z]{1,2})/g;
        var flag = reg.exec(region_url);
        var arr = new Array("US", "Walmart ", "Canada ", "UK ", "Germany ", "France ", "Itany ", "Spain ");
        if (flag == null) {
            flag = 'U';
        } else {
            flag = flag[1];
            if (flag == "US ") {
                flag = "U ";
            }
            if (flag == "UK ") {
                flag = "K ";
            }
        }
        var str = "UWCKGFIS ";
        var num = str.indexOf(flag);
        $("#product - show - title ").find("li ").removeClass('active');
        $("#product - show - title ").find("li ").eq(num).addClass('active');
        $("#product - show - all ").find("ul ").each(function() {
            $(this).hide();
        });
        $("#product - show - all ").find("ul ").eq(num).show();
        $("#product - show - title ").find("li ").each(function(i) {
            $(this).bind({
                "click ": function() {
                    if (arr[i] == undefined) {
                        window.location.href = ". / Japan ";
                    } else {
                        history.pushState({}, "Etekcity - Welcome to the Woodsammer Program ", " ? " + arr[i]);
                        $("#product - show - title ").find("li ").removeClass('active');
                        $(this).addClass('active');
                        $("#product - show - all ").find("ul ").each(function() {
                            $(this).hide();
                        });
                        $("#product - show - all ").find("ul ").eq(i).show();
                    }
                }
            });
        });

        $("#submit ").click(function() {
            if ($("input[name='first_name']").val() === "") {
                alert("Please enter your first name!");
                return false;
            } else if ($("input[name='last_name']").val() === "") {
                alert("Please enter your last name!");
                return false;
            } else if ($("input[name='email']")[0] && !($("input[name='email']").val().match(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/))) {
                alert("Please enter your email right!");
                return false;
            } else if ($("input[name='address_line_1']").val() === "") {
                alert("Please enter your address!");
                return false;
            } else if ($("input[name='state']").val() === "") {
                alert("please enter your state!");
                return false;
            } else if ($("input[name='city']").val() === "") {
                alert("please enter your city!");
                return false;
            } else if ($("input[name='zipcode']").val() === "") {
                alert("please input your zipcode!");
                return false;
            } else if ($("input[name='amazon']")[0] && $("input[name='amazon']").val() === "") {
                alert("please input your link!");
                return false;
            } else if ($("input[name='woodsammer_id']")[0] && $("input[name='woodsammer_id']").val() === "") {
                alert("Please input your Woodsammer ID!");
                return false;
            } else if ($("input[name='product_id']")[0] && $("input[name='product_id']").val() === "") {
                alert("Please input the Product ID of the free product you want!");
                return false;
            }
            return true;
        });
    });

    // (function(d, s, id) {
    //     var js, fjs = d.getElementsByTagName(s)[0];
    //     if (d.getElementById(id)) {
    //         return;
    //     }
    //     js = d.createElement(s);
    //     js.id = id;
    //     js.src = "/ / connect.facebook.net / en_US / all.js # xfbml = 1 ";
    //     fjs.parentNode.insertBefore(js, fjs);
    // }(document, 'script', 'facebook-jssdk'));
});