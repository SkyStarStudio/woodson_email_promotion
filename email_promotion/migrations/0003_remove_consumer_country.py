# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('email_promotion', '0002_auto_20150929_0924'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='consumer',
            name='country',
        ),
    ]
