# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('email_promotion', '0005_banner'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Banner',
        ),
        migrations.AddField(
            model_name='productmodel',
            name='product_id',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
