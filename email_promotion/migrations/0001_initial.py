# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Consumer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('full_name', models.CharField(max_length=255)),
                ('email_address', models.CharField(max_length=255)),
                ('mobile', models.CharField(max_length=15)),
                ('street', models.CharField(max_length=255)),
                ('city', models.CharField(max_length=255)),
                ('state', models.CharField(max_length=255)),
                ('zipcode', models.CharField(max_length=10)),
                ('country', models.CharField(max_length=255)),
                ('interests', models.CharField(max_length=255, null=True, blank=True)),
                ('identity', models.CharField(max_length=255, null=True, blank=True)),
                ('source', models.CharField(max_length=255, null=True, blank=True)),
                ('amazon_profile_url', models.CharField(max_length=255)),
                ('personal_url', models.CharField(max_length=255, null=True, blank=True)),
                ('created_date', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='ProductModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('url', models.CharField(max_length=255)),
                ('image', models.ImageField(upload_to=b'products/')),
                ('online', models.BooleanField(default=True)),
            ],
        ),
    ]
