# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('email_promotion', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='consumer',
            old_name='street',
            new_name='address_line_1',
        ),
        migrations.RenameField(
            model_name='consumer',
            old_name='personal_url',
            new_name='address_line_2',
        ),
        migrations.AddField(
            model_name='consumer',
            name='first_name',
            field=models.CharField(default='Sample Fist Name', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='consumer',
            name='last_name',
            field=models.CharField(default='last_name', max_length=255),
            preserve_default=False,
        ),
    ]
