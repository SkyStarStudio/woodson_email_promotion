# coding=UTF_8
from django.http import HttpResponseBadRequest, JsonResponse, HttpResponse
from django.template.response import TemplateResponse
from .models import ProductModel, Consumer, Member
import requests


def new_consumer(request):
    """
    显示页面
    """
    products_queryset = ProductModel.objects.filter(online=True)
    return TemplateResponse(request, 'new_consumer.html', {"products": products_queryset})

def member(request):
    """
    显示页面
    """
    return TemplateResponse(request, 'member.html')


def new_submit(request):
    """
    用户post数据
    * URL: /consumer/
    * MET: POST
    * PARAM: full_name, email, phone_number, street, city, state, zipcode, interests, identity, source, amazon, personal_url
    * RET: 200 or 400
    """
    if request.method == 'GET':
        return HttpResponseBadRequest()
    else:
        data = request.POST
        recaptcha = data.get('g-recaptcha-response')
        if not recaptcha:
            return HttpResponseBadRequest()
        else:
            verify_result = verify_user_response(recaptcha)
            if not verify_result['success']:
                return HttpResponseBadRequest()

        first_name=data.get('first_name')
        last_name=data.get('last_name')
        consumer = Consumer(
            first_name=first_name,
            last_name=last_name,
            full_name=last_name+', '+first_name,
            email_address=data.get('email'),
            mobile=data.get('phone_number'),
            address_line_1=data.get('address_line_1'),
            address_line_2=data.get('address_line_2', ''),
            city=data.get('city'),
            state=data.get('state'),
            zipcode=data.get('zipcode'),
            interests=', '.join(data.getlist('interests', '')),
            identity=', '.join(data.getlist('identity', '')),
            source=', '.join(data.getlist('source', '')),
            amazon_profile_url=data.get('amazon', '')
        )
        consumer.save()
        if consumer.id:
            return JsonResponse({
                'status': 200,
                'message': 'Thanks for your interest in the Woodsammer Program! We\'ve successfully received your online application. We will respond to you within three business days.',
            })
        else:
            return JsonResponse({
                'status': 400,
                'message': 'Sorry, we encounterd some error on this application, please double check all the fields and submit the form again.',
            })


def member_submit(request):
    """
    用户post数据
    * URL: /consumer/
    * MET: POST
    * PARAM: full_name, email, phone_number, street, city, state, zipcode, interests, identity, source, amazon, personal_url
    * RET: 200 or 400
    """
    if request.method == 'GET':
        return HttpResponseBadRequest()
    else:
        data = request.POST
        recaptcha = data.get('g-recaptcha-response')
        if not recaptcha:
            return HttpResponseBadRequest()
        else:
            verify_result = verify_user_response(recaptcha)
            if not verify_result['success']:
                return HttpResponseBadRequest()

        member = Member(
            first_name=data.get('first_name'),
            last_name=data.get('last_name'),
            address_line_1=data.get('address_line_1'),
            address_line_2=data.get('address_line_2', ''),
            city=data.get('city'),
            state=data.get('state'),
            zipcode=data.get('zipcode'),
            woodsammer_id=data.get('woodsammer_id'),
            product_id=data.get('product_id'),
        )
        member.save()
        if member.id:
            return JsonResponse({
                'status': 200,
                'message': 'Thanks for your interest in the Woodsammer Program! We\'ve successfully received your Product Request Form. We will respond to you within three business days.',
            })
        else:
            return JsonResponse({
                'status': 400,
                'message': 'Sorry, we encounterd some error on this application, please double check all the fields and submit the form again.',
            })

def verify_user_response(recaptcha):
    url = "https://www.google.com/recaptcha/api/siteverify"
    secret = "6LfcmQ0TAAAAAIPUQAWvT5pDFly0Vccus6wTe415"
    data = {
        "secret": secret,
        "response": recaptcha,
    }
    response = requests.post(url, data).json()
    return response
