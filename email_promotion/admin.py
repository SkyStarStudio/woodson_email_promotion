# coding=UTF-8
from django.contrib import admin
from django.utils.encoding import smart_str
from django.http import HttpResponse
from datetime import datetime
from .models import ProductModel, Consumer, Member
import csv, xlwt


DATE_FORMAT = "%Y_%m_%d"
TIME_FORMAT = "%Y-%m-%d %H:%M:%S"


@admin.register(ProductModel)
class ProductAdmin(admin.ModelAdmin):
    search_field = ('title', )
    list_display = ('title', 'product_id', 'url', 'image', 'online')


@admin.register(Consumer)
class ConsumerAdmin(admin.ModelAdmin):
    actions = ["export_csv", "export_xls"]
    search_field = ('first_name', 'last_name', 'full_name', 'email_address',)
    list_display = ('first_name', 'last_name', 'email_address', 'mobile', 'created_date')

    # export functions
    def export_csv(self, request, queryset):
        d = datetime.now().strftime(DATE_FORMAT)
        response = HttpResponse()
        response['Content-Type'] = 'text/csv'
        response['Content-Disposition'] = "attachment; filename=consumer_%s.csv" % d
        writer = csv.writer(response, csv.excel)
        writer.writerow([
            smart_str("First Name"),
            smart_str("Last Name"),
            smart_str("Full Name"),
            smart_str("Email"),
            smart_str("Phone"),
            smart_str("Address Line 1"),
            smart_str("Address Line 2"),
            smart_str("City"),
            smart_str("State"),
            smart_str("Zip"),
            smart_str("Interests"),
            smart_str("Identity"),
            smart_str("Source"),
            smart_str("Amazon Profile"),
            smart_str("Date")
        ])
        for consumer in queryset:
            writer.writerow([
                smart_str(consumer.first_name),
                smart_str(consumer.last_name),
                smart_str(consumer.full_name),
                smart_str(consumer.email_address),
                smart_str(consumer.mobile),
                smart_str(consumer.address_line_1),
                smart_str(consumer.address_line_2),
                smart_str(consumer.city),
                smart_str(consumer.state),
                smart_str(consumer.zipcode),
                smart_str(consumer.interests),
                smart_str(consumer.identity),
                smart_str(consumer.source),
                smart_str(consumer.amazon_profile_url),
                smart_str(consumer.created_date.strftime(TIME_FORMAT))
            ])
        return response
    export_csv.short_description = u"Export CSV"

    def export_xls(self, request, queryset):
        d = datetime.now().strftime(DATE_FORMAT)
        response = HttpResponse()
        response['Content-Type'] = 'application/ms-excel'
        response['Content-Disposition'] = 'attachment; filename=consumer_%s.xls' % d
        wb = xlwt.Workbook(encoding="UTF-8")
        ws = wb.add_sheet("Consumer")
        row_num = 0
        columns = [
            (u"First Name", 255),
            (u"Last Name", 255),
            (u"Full Name", 255),
            (u"Email", 255),
            (u"Phone", 15),
            (u"Address Line 1", 255),
            (u"Address Line 2", 255),
            (u"City", 255),
            (u"State", 255),
            (u"Zip", 10),
            (u"Interests", 255),
            (u"Identity", 255),
            (u"Source", 255),
            (u"Amazon Profile", 255),
            (u"Date", 255),
        ]
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        for col_num in xrange(len(columns)):
            ws.write(row_num, col_num, columns[col_num][0], font_style)
            ws.col(col_num).width = columns[col_num][1]

        font_style = xlwt.XFStyle()
        font_style.alignment.wrap = 1

        for consumer in queryset:
            row_num += 1
            row = [
                consumer.first_name,
                consumer.last_name,
                consumer.full_name,
                consumer.email_address,
                consumer.mobile,
                consumer.address_line_1,
                consumer.address_line_2,
                consumer.city,
                consumer.state,
                consumer.zipcode,
                consumer.interests,
                consumer.identity,
                consumer.source,
                consumer.amazon_profile_url,
                consumer.created_date.strftime(TIME_FORMAT)
            ]
            for col_num in xrange(len(row)):
                ws.write(row_num, col_num, row[col_num], font_style)

        wb.save(response)
        return response
    export_xls.short_description = u"Export XLS"


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    actions = ["export_csv", "export_xls"]
    search_field = ('first_name', 'last_name', 'woodsammer_id',)
    list_display = ('woodsammer_id', 'first_name', 'last_name', 'product_id', 'created_date',)

    # export functions
    def export_csv(self, request, queryset):
        d = datetime.now().strftime(DATE_FORMAT)
        response = HttpResponse()
        response['Content-Type'] = 'text/csv'
        response['Content-Disposition'] = "attachment; filename=member_%s.csv" % d
        writer = csv.writer(response, csv.excel)
        writer.writerow([
            smart_str("Woodsammer ID"),
            smart_str("First Name"),
            smart_str("Last Name"),
            smart_str("Product ID"),
            smart_str("Address Line 1"),
            smart_str("Address Line 2"),
            smart_str("City"),
            smart_str("State"),
            smart_str("Zip"),
            smart_str("Create Date")
        ])
        for member in queryset:
            writer.writerow([
                smart_str(member.woodsammer_id),
                smart_str(member.first_name),
                smart_str(member.last_name),
                smart_str(member.product_id),
                smart_str(member.address_line_1),
                smart_str(member.address_line_2),
                smart_str(member.city),
                smart_str(member.state),
                smart_str(member.zipcode),
                smart_str(member.created_date.strftime(TIME_FORMAT))
            ])
        return response
    export_csv.short_description = u"Export CSV"

    def export_xls(self, request, queryset):
        d = datetime.now().strftime(DATE_FORMAT)
        response = HttpResponse()
        response['Content-Type'] = 'application/ms-excel'
        response['Content-Disposition'] = 'attachment; filename=member_%s.xls' % d
        wb = xlwt.Workbook(encoding="UTF-8")
        ws = wb.add_sheet("Member")
        row_num = 0
        columns = [
            (u"Woodsammer ID", 255),
            (u"First Name", 255),
            (u"Last Name", 255),
            (u"Product ID", 255),
            (u"Address Line 1", 255),
            (u"Address Line 2", 255),
            (u"City", 255),
            (u"State", 255),
            (u"Zip", 10),
            (u"Create Date", 255),
        ]
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        for col_num in xrange(len(columns)):
            ws.write(row_num, col_num, columns[col_num][0], font_style)
            ws.col(col_num).width = columns[col_num][1]

        font_style = xlwt.XFStyle()
        font_style.alignment.wrap = 1

        for member in queryset:
            row_num += 1
            row = [
                member.woodsammer_id,
                member.first_name,
                member.last_name,
                member.product_id,
                member.address_line_1,
                member.address_line_2,
                member.city,
                member.state,
                member.zipcode,
                member.created_date.strftime(TIME_FORMAT),
            ]
            for col_num in xrange(len(row)):
                ws.write(row_num, col_num, row[col_num], font_style)

        wb.save(response)
        return response
    export_xls.short_description = u"Export XLS"