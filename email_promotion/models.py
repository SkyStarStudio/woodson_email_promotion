# coding=UTF-8
from django.db import models


class ProductModel(models.Model):
    title = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    product_id = models.CharField(max_length=255)
    image = models.ImageField(upload_to='products/')
    online = models.BooleanField(default=True)

    def __str__(self):
        return " ".join([self.title, self.url])


class Consumer(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    full_name = models.CharField(max_length=255)
    email_address = models.CharField(max_length=255)
    mobile = models.CharField(max_length=15)
    address_line_1 = models.CharField(max_length=255)
    address_line_2 = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    zipcode = models.CharField(max_length=10)
    interests = models.CharField(max_length=255, blank=True, null=True)
    identity = models.CharField(max_length=255, blank=True, null=True)
    source = models.CharField(max_length=255, blank=True, null=True)
    amazon_profile_url = models.CharField(max_length=255)
    created_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return " ".join([self.full_name, self.email_address, self.mobile])


class Member(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    address_line_1 = models.CharField(max_length=255)
    address_line_2 = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    zipcode = models.CharField(max_length=10)
    woodsammer_id = models.CharField(max_length=255)
    product_id = models.CharField(max_length=255)
    created_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return " ".join([self.first_name, self.last_name, self.woodsammer_id])
